import { StatusBar } from 'expo-status-bar';
import React, { useEffect, useState } from 'react';
import { FlatList, StyleSheet, Text, View } from 'react-native';
import { data } from './src/services/data.js'

const Item = ({ item }) => (
 
  <View style={{
    flexDirection:'row',
     borderWidth:1, 
     marginBottom:2,
      justifyContent:"space-between",
      padding:5,
      borderRadius:5,
      backgroundColor:  'rgba(205, 205, 205, 0.8)'

    }}>

    <Text style={{width:100}}>{item.name} </Text>
    <Text style={{width: 30}}>{item.base}</Text>
    <Text style={{width: 100}}>{item.price}</Text>
    <Text style={styles.titleItem}>{item.quote} </Text>
  </View>




);

export default function App() {
  const [lista, setLista] = useState(null)
  useEffect(() => {
    cargar().then(()=>{})
  }, [])
  async function cargar() {

    const datos = await data();
    setLista(datos.data)
  }
  const renderItem = ({ item }) => (
    <Item
      item={item}

      style={{with: "100%"}}
    />
  );
  return (
    <View style={styles.container}>
      {lista
        ? <>
        <FlatList
        data={lista}
        renderItem={renderItem}
        keyExtractor={(item) => Math.random().toString()}
      />
        </> 
        :<><Text>Cargando</Text></>
      }
  

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',

  },
  titleItem: {},
  observacionItem: {}
});
